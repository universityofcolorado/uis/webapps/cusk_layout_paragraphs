# Module - Layout Paragraphs

Enabling the CU Layout Paragraphs Bundle makes available a layout paragraph type along with several for common content patterns, that can be added to content types.

* Enable CU Layout Paragraphs (dependencies: Layout Paragraphs, Paragraphs, Entity Reference Revisions, Field, File, Layout Discovery).
* In structure view -> Paragraph types, the module provides the following Paragraph Types: 
  * Layout: provides different layout options for your content(one/two/three columns)
  * WYSIWYG: a rich text editor field utilizing Drupal CKEditor
  * View Reference: Allows selection of a reference view for display
